# coding: utf8
import scrapy
import re
import json
import pprint
import csv
import sys
import logging
from parser_target.items import ParserTargetItem

class ParserTargetSpider(scrapy.Spider):

    name = "target"

    def start_requests(self):
        url = 'https://www.target.com/api/content-publish/pages/v1/slingshot/category/5xsxf?target=tablet&channel=web&page_id=/c/5xsxf&context=geo,12901|44.702338|-73.491875|NY|US'
        yield scrapy.Request(url=url, callback=self.parse_category)


    def parse_category(self, response):
        body = response.body
        jBody = json.loads(body)
        # pprint.pprint(jBody)
        for slots in jBody['slots']:
            slot = jBody['slots'][slots]
            if 'content' in slot:
               if 'shop_by' in slot['content']:
                   shop = slot['content']['shop_by']
                   for shp in shop:
                       if 'link_url' in shp:
                           # result = re.search(r'^.+N-(\S+)\?',shp['link_url']).group(1)
                           url ='https://www.target.com/api/content-publish/taxonomy/v1/seo?url='+shp['link_url']+'&children=true&breadcrumbs=true&content=true'
                           yield scrapy.Request(url=url, callback=self.parse_subcategory)

    def parse_subcategory(self, response):
        body = response.body
        jBody = json.loads(body)
        # pprint.pprint(jBody)
        if 'children' in jBody:
            for child in jBody['children']:
                rUrl = child['seo_data']['canonical_url']
                url = 'https://www.target.com/api/content-publish/taxonomy/v1/seo?url=' + rUrl + '&children=true&breadcrumbs=true&content=true'
                yield scrapy.Request(url=url, callback=self.parse_subcategory)
        else:
            nodId = jBody['node_id']
            url = 'https://redsky.target.com/v1/plp/search?count=24&offset=0&category='+nodId+'&channel=web&pageId=/c/'
            yield scrapy.Request(url=url, callback=self.parse_sub_sub_category)

    def parse_sub_sub_category(self, response):
        body = response.body
        jBody = json.loads(body)
        # pprint.pprint(jBody)
        if 'search_response' in jBody:
            search_response = jBody['search_response']
            cnt = int(search_response['metaData'][1]['value'])
            if cnt > 24:
                category = search_response['metaData'][6]['value']
                for offset in range(24, cnt+24, 24):
                    url = 'https://redsky.target.com/v1/plp/search?count=24&offset='+str(offset)+'&category=' + category + '&channel=web&pageId=/c/'
                    yield scrapy.Request(url=url, callback=self.parse_sub_sub_category)
            items = search_response['items']['Item']
            if len(items)>0:
                for item in items:
                    if 'url' in item and 'title' in item:
                        product = ParserTargetItem()
                        product['url'] = item['url']
                        product['id'] = item['tcin']
                        if len(item['images'])>0:
                            product['img'] = item['images'][0]['base_url'] + item['images'][0]['primary']
                        else:
                            product['img'] = ''
                        product['produckt_group'] = search_response['breadCrumb_list'][0]['breadCrumbValues'][1]['label']
                        product['name'] = item['title']
                        if 'list_price' in item:
                            price = item['list_price']['price']
                            if price == 0:
                                product['price'] = item['list_price']['min_price']
                            else:
                                product['price'] = price
                        else:
                            product['price'] = 0
                        if product['produckt_group'] != 'holiday shop':
                            yield product




















